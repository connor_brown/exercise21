﻿using System;
// a)	Create a simple for loop that prints out the
// 	numbers 1 to 5.

// b)	Create a simple while loop that prints out the
// 	numbers 1 to 5.


// c)	Create a for loop and then a while loop that
// 	displays only the even numbers from 1 to 20

// d)	Create a “do while loop” that has an index
// greater than the counter. Print a line that
// 	prints the value of both, indicating that the
// 	index is bigger. Note: you will also need to
// 	use an if statement.

namespace exercise21
{
    class Program
    {
        static void Main(string[] args)
        {
            int index = 0;
             int counter = 20;
            //  while (index < counter)
            //  {
            //      var a = (index +1);
            //      Console.WriteLine($"this is a line number {a}");
            //      index++;
            //  }
             while(index < counter)
             {
                 var a =index +1;
                 
                 index++;
                 if(a%2==0)
                 {
                     Console.WriteLine($"this is a line number {a}");

                 }
                 
             }
            

            
             
            // for(var i = 0; i < counter; i++)
            // {
            //     var a = i + 1;
            //     Console.WriteLine($"this is line number {a}");
            // }
        }
    }
}
